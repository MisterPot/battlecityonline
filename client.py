from battle_city.network.client_game import ClientGame


if __name__ == '__main__':
    game = ClientGame(
        host='localhost',
        target_host='localhost',
        target_tcp_port=8181,
    )
    game.run()
