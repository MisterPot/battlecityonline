from battle_city.network.server_game import ServerGame
from battle_city.network.network_manager import NetworkManager


if __name__ == '__main__':
    game = ServerGame(
        host='localhost',
        tcp_port=8181,
        manager_receive_function=NetworkManager.receive_tcp_message_buffered,
        frames_per_second=30,
    )
    game.run()
