import pygame.event


# Object identifier codes
##########################

class Identifier:
    TILE = 0
    TANK = 2
    BULLET = 4
    ANIMATION = 5

##########################


# Objects subtypes codes
##########################

class TileCodes:
    BRICK = 0
    IRON = 1
    TREE = 2
    WATER_1 = 3
    WATER_2 = 4
    WATER_3 = 5
    CONCRETE = 6


class BulletCodes:
    BULLET = 0


class TankCodes:
    SIMPLE_GOLD_TANK = 0


class AnimationCodes:
    SPAWN = 0
    PROTECTION = 1

##########################


# Help stuff
##########################

class DirectionCodes:
    UP = 0
    LEFT = 1
    DOWN = 2
    RIGHT = 3


class Angles:
    UP = 0
    RIGHT = -90
    LEFT = 90
    DOWN = 180


class TankControl:
    CANT = 0
    CAN = 1


class Alive:
    NOT = 0
    YES = 1


class CustomEvents:
    CREATE_OBJECT = pygame.event.custom_type()
    MOVE_OBJECT = pygame.event.custom_type()
    REMOVE_OBJECT = pygame.event.custom_type()


class Actions:

    GET_UUID = 0
    GET_EVENTS = 1
    GET_MAP = 2
    GET_FRAME_RATE = 3
    SET_UDP = 4
    ACCEPTED = 5
    ERROR = 6
    PING = 7

##########################
