from __future__ import annotations
from typing import Union, Protocol
import pathlib


class PathResolver:

    def __init__(self, value: Union[str, pathlib.Path], **kwargs):
        self.value = value

        for key, value in kwargs.items():
            setattr(self, key, self.connect(value))

    def __get__(self, instance, owner):
        return self

    def string(self) -> str:
        return str(self.value)

    def path(self) -> pathlib.Path:
        return pathlib.Path(self.value)

    def connect(self, path: Union[str, pathlib.Path]) -> PathResolver:
        return PathResolver(self.path() / pathlib.Path(path))

    def to_tree(self, **kwargs) -> PathResolver:
        return PathResolver(self.value, **kwargs)

    def __set__(self, instance, value):
        self.value = value


class Tiles(Protocol):
    BRICK_FULL: PathResolver
    IRON: PathResolver
    TREE: PathResolver
    WATER_1: PathResolver
    WATER_2: PathResolver
    WATER_3: PathResolver
    CONCRETE: PathResolver


class Tanks(Protocol):
    SIMPLE_GOLD_TANK: PathResolver


class Misc(Protocol):
    BULLET: PathResolver
    SPAWN_1: PathResolver
    SPAWN_2: PathResolver
    SPAWN_3: PathResolver
    SPAWN_4: PathResolver
    PROTECTION_1: PathResolver
    PROTECTION_2: PathResolver


class Environment:

    ROOT = PathResolver(pathlib.Path(__file__).parent)
    IMAGES = ROOT.connect('images')
    TILES: Tiles = IMAGES.connect('tiles').to_tree(
        BRICK_FULL='brick.png',
        IRON='iron_tile.png',
        TREE='tree_tile.png',
        WATER_1='water1.png',
        WATER_2='water2.png',
        WATER_3='water3.png',
        CONCRETE='concrete.png'
    )
    TANKS: Tanks = IMAGES.connect('tanks').to_tree(
        SIMPLE_GOLD_TANK='gt.png',
    )
    MISC: Misc = IMAGES.connect('other').to_tree(
        BULLET='bullet.png',
        SPAWN_1='spawn_1.png',
        SPAWN_2='spawn_2.png',
        SPAWN_3='spawn_3.png',
        SPAWN_4='spawn_4.png',
        PROTECTION_1='protection_1.png',
        PROTECTION_2='protection_2.png'
    )


def reverse_dict(dictionary: dict) -> dict:
    return {value: key for key, value in dictionary.items()}


def expect_difference(
        input_number: int,
        operable_number: int,
        expected: int,
        difference: int = 0,
) -> int:

    biggest_number = input_number if input_number > expected else expected
    expectation = biggest_number % operable_number

    if expectation != 0:
        raise ValueError(
            f'Cant expect difference on numbers - {input_number} and {expected}. '
            f'Because {biggest_number} % {operable_number} = {expectation}'
        )

    operation = int.__sub__ if expected < input_number else int.__add__

    if input_number == expected:
        return difference
    else:
        return expect_difference(
            input_number=operation(input_number, operable_number),
            operable_number=operable_number,
            expected=expected,
            difference=operation(difference, operable_number),
        )