import json
from abc import (
    ABC,
    abstractmethod
)
from typing import (
    List,
    Any,
    Dict,
    Callable
)

import pygame.sprite
import pygame

from .enum import (
    TileCodes,
    Identifier,
    BulletCodes,
    TankControl,
    CustomEvents,
)
from .objects.tank import (
    Tank,
    TankAttachedAnimation,
    ManyTimesAnimation
)
from .objects.bullet import Bullet
from .objects.tile import Tile
from .events import create_create_event


class CommandContext(ABC):

    @abstractmethod
    def run(self) -> None:
        ...


class SpawnTank(CommandContext):

    def __init__(
            self,
            code: int,
            start_x: int,
            start_y: int,
            game_state,
            can_control: int = None
    ):
        self.code = code
        self.start_x = start_x
        self.start_y = start_y
        self.game_state = game_state
        self.can_control = can_control or TankControl.CANT

        self.tank = None
        self.events = []

    def run(self) -> None:
        self.tank = Tank.create_from_code(
            code=self.code,
            start_y_cord=self.start_y,
            start_x_cord=self.start_x,
            game_state=self.game_state,
        )
        self.tank.can_control = self.can_control
        creation_event = create_create_event(
            identifier=Identifier.TANK,
            position_y=self.start_y,
            position_x=self.start_x,
            object_identifier=self.code,
            attributes={
                'DIRECTION': self.tank.DIRECTION,
                'uuid': self.tank.uuid
            }
        )
        self.events.append()


class SpawnTile(CommandContext):

    def __init__(
            self,
            start_x: int,
            start_y: int,
            tile_code: int,
            game_state,
            storage: list,
            spawn_function: Callable = None
    ):
        self.start_x = start_x
        self.start_y = start_y
        self.tile_code = tile_code
        self.game_state = game_state
        self.storge = storage
        self.spawn_function = spawn_function or Tile.create_from_code

    def run(self) -> None:
        self.storge.append(
            self.spawn_function(
                code=self.tile_code,
                start_x_cord=self.start_x,
                start_y_cord=self.start_y,
                game_state=self.game_state
            )
        )


class TileSpawnerCommandContext(CommandContext):

    def __init__(
            self,
            start_x: int,
            start_y: int,
            size_x: int,
            size_y: int,
            game_state,
            tile_code: int,
            spawn_function: Callable = None
    ):
        self.size_x = size_x
        self.size_y = size_y
        self.start_x = start_x
        self.start_y = start_y
        self.game_state = game_state
        self.tile_code = tile_code
        self.spawn_function = spawn_function or Tile.create_from_code
        self.tiles = self.create_tiles()

    @abstractmethod
    def create_tiles(self) -> List[Tile]:
        ...

    def validate(self) -> None:
        check_group = pygame.sprite.Group(*self.tiles)

        collisions = pygame.sprite.groupcollide(
            groupa=self.game_state.map.objects,
            groupb=check_group,
            dokilla=False,
            dokillb=False,
            collided=pygame.sprite.collide_mask
        )

        if collisions:
            raise ValueError('Cant spawn square there')

    def run(self) -> None:
        self.validate()
        self.game_state.map.objects.add(*self.tiles)


class SpawnSquareCommand(TileSpawnerCommandContext):

    def create_tiles(self) -> List[Tile]:

        tiles = []
        current_x = self.start_x
        current_y = self.start_y

        for _ in range(self.size_y):
            for _ in range(self.size_x):
                SpawnTile(
                    start_x=current_x,
                    start_y=current_y,
                    tile_code=self.tile_code,
                    game_state=self.game_state,
                    storage=tiles,
                    spawn_function=self.spawn_function
                ).run()
                current_x += Tile.OBJECT_WIDTH

            current_x = self.start_x
            current_y += Tile.OBJECT_HEIGHT

        return tiles


class SpawnRingCommand(TileSpawnerCommandContext):

    def create_tiles(self) -> List[Tile]:
        tiles = []

        current_x = self.start_x
        current_y = self.start_y
        max_x = current_x + Tile.OBJECT_WIDTH * (self.size_x - 1)
        max_y = current_y + Tile.OBJECT_HEIGHT * (self.size_y - 1)

        for _ in range(self.size_y):
            for _ in range(self.size_x):

                if (
                        current_y in [self.start_y, max_y]
                        or current_x in [self.start_x, max_x]
                ):
                    SpawnTile(
                        start_x=current_x,
                        start_y=current_y,
                        tile_code=self.tile_code,
                        storage=tiles,
                        game_state=self.game_state,
                        spawn_function=self.spawn_function
                    ).run()

                current_x += Tile.OBJECT_WIDTH

            current_x = self.start_x
            current_y += Tile.OBJECT_HEIGHT

        return tiles


class ParseClientDataCommand(CommandContext):

    def __init__(
            self,
            data_string: str,
            game_state
    ):
        self.data_string = data_string
        self.game_state = game_state
        self.parsed_objects = []

    def run(self) -> None:
        chunks = self.data_string.split('|')
        self.game_state.map.objects.empty()
        for chunk in chunks:
            command = ParseObjectCommand(
                data_list=chunk,
                storage=self.parsed_objects,
                game_state=self.game_state
            )
            command.run()


class ParseObjectCommand(CommandContext):

    def __init__(
            self,
            data_list: str,
            storage: list,
            game_state,
    ):
        self.data_list = data_list
        self.storage = storage
        self.game_state = game_state

        self.parse()

    def parse(self) -> None:
        base_bytes_length = 6
        self.base_bytes = list(map(int, self.data_list.split('!')))
        self.additional_bytes = self.base_bytes[base_bytes_length:]
        self.data_length = self.base_bytes[0]
        self.identifier = self.base_bytes[1]
        self.object_code = self.base_bytes[2]
        self.pos_x = self.base_bytes[3]
        self.pos_y = self.base_bytes[4]
        self.is_spawned = self.base_bytes[5]

    def print_stack(self) -> None:
        print(f"""
            Parsed object length = {self.data_length}
            Object identifier = {self.identifier}
            Object identification code = {self.object_code}
            Object X position = {self.pos_x}
            Object Y position = {self.pos_y}
            Object alive = {self.is_spawned}
            Additional flags = {self.additional_bytes}
        """)

    def run(self) -> None:

        if self.identifier == Identifier.TILE:

            if self.object_code == TileCodes.BRICK:
                self.game_state.map.objects.add(Tile.create_tile_part(
                    game_state=self.game_state,
                    start_x_cord=self.pos_x,
                    start_y_cord=self.pos_y,
                    code=self.object_code
                ))
            else:
                self.game_state.map.objects.add(Tile.create_from_code(
                    code=self.object_code,
                    start_y_cord=self.pos_y,
                    start_x_cord=self.pos_x,
                    game_state=self
                ))

        elif self.identifier == Identifier.TANK:
            direction = int(self.additional_bytes[0])
            ident = int(self.additional_bytes[1])
            tank = Tank.create_from_code(
                code=self.object_code,
                game_state=self.game_state,
                start_y_cord=self.pos_y,
                start_x_cord=self.pos_x,
            )
            tank.rotate(direction=direction)
            tank.uuid = ident
            tank.can_control = TankControl.CANT
            self.game_state.map.objects.add(tank)

        elif self.identifier == Identifier.BULLET:
            direction = int(self.additional_bytes[0])
            ident = int(self.additional_bytes[1])
            bullet = Bullet.create_from_code(
                code=BulletCodes.BULLET,
                start_x_cord=self.pos_x,
                start_y_cord=self.pos_y,
                game_state=self.game_state
            )
            bullet.rotate(direction)
            bullet.uuid = ident
            self.game_state.map.objects.add(bullet)


class FindSpriteByCords(CommandContext):

    def __init__(
            self,
            expected_x: int,
            expected_y: int,
            game_state,
    ):
        self.expected_x = expected_x
        self.expected_y = expected_y
        self.game_state = game_state
        self.result = None

    def run(self) -> None:
        sprites = self.game_state.map.objects.sprites()

        for sprite in sprites:
            if not (
                    self.expected_x == sprite.rect.x and
                    self.expected_y == sprite.rect.y
            ):
                continue
            self.result = sprite


class FindSpriteByUUID(CommandContext):

    def __init__(self, uuid: int, game_state):
        self.uuid = uuid
        self.result = None
        self.game_state = game_state

    def run(self) -> None:
        sprites = self.game_state.map.objects.sprites()
        for sprite in sprites:
            uuid_ = getattr(sprite, 'uuid', None)
            if self.uuid != uuid_:
                continue
            self.result = sprite


class DumpEvents(CommandContext):

    def __init__(self, event_list: List[pygame.event.Event]):
        self.event_list = event_list
        self.dumped = None

    def run(self) -> None:
        list_of_dumps = []
        for event in self.event_list:
            command = DumpEvent(event=event)
            command.run()
            list_of_dumps.append(command.dumped)
        self.dumped = f"[{', '.join(list_of_dumps)}]"


class DumpEvent(CommandContext):

    def __init__(self, event: pygame.event.Event):
        self.event = event
        self.dumped = None

    def run(self) -> None:
        self.dumped = json.dumps({
            'type': self.event.type,
            **self.event.dict
        })


class ApplyRemoveEvent(CommandContext):

    def __init__(
            self,
            object_x: int,
            object_y: int,
            attributes: Dict[str, int],
            game_state,
    ):
        self.object_x = object_x
        self.object_y = object_y
        self.game_state = game_state
        self.attributes = attributes

    def run(self) -> None:
        _uuid = self.attributes.get('uuid')

        command = FindSpriteByCords(
            expected_x=self.object_x,
            expected_y=self.object_y,
            game_state=self.game_state
        )

        if _uuid:
            command = FindSpriteByUUID(
                uuid=_uuid,
                game_state=self.game_state
            )

        command.run()
        if not command.result:
            return
        command.result.kill()


class ApplyMoveEvent(CommandContext):

    def __init__(
            self,
            from_x: int,
            from_y: int,
            to_x: int,
            to_y: int,
            attributes: Dict[str, Any],
            game_state
    ):
        self.from_x = from_x
        self.from_y = from_y
        self.to_x = to_x
        self.to_y = to_y
        self.game_state = game_state
        self.attributes = attributes

    def run(self) -> None:
        _uuid = self.attributes.get('uuid')
        command = FindSpriteByCords(
            expected_x=self.from_x,
            expected_y=self.from_y,
            game_state=self.game_state
        )
        if _uuid:
            command = FindSpriteByUUID(
                uuid=_uuid,
                game_state=self.game_state
            )
        command.run()

        if not command.result:
            return

        command.result.rect.topleft = (self.to_x, self.to_y)
        command.result.rotate(
            self.attributes.get('DIRECTION', command.result.DIRECTION)
        )


class ApplyCreateEvent(CommandContext):

    def __init__(
            self,
            identifier: int,
            object_identifier: int,
            position_x: int,
            position_y: int,
            attributes: Dict[str, Any],
            game_state
    ):
        self.identifier = identifier
        self.object_identifier = object_identifier
        self.position_x = position_x
        self.position_y = position_y
        self.attributes = attributes
        self.game_state = game_state

    def run(self) -> None:

        if self.identifier == Identifier.BULLET:

            bullet = Bullet.create_from_code(
                code=self.object_identifier,
                start_x_cord=self.position_x,
                start_y_cord=self.position_y,
                game_state=self.game_state
            )
            bullet.rotate(
                self.attributes.get('DIRECTION', bullet.DIRECTION)
            )
            bullet.uuid = self.attributes.get('uuid', bullet.uuid)
            bullet.future_collisions = lambda *_, **kwargs: None
            self.game_state.map.objects.add(bullet)

        elif self.identifier == Identifier.TANK:
            print('creating tank')
            tank = Tank.create_from_code(
                code=self.object_identifier,
                start_y_cord=self.position_y,
                start_x_cord=self.position_x,
                game_state=self.game_state
            )
            tank.rotate(
                self.attributes.get('DIRECTION', tank.DIRECTION)
            )
            tank.uuid = self.attributes.get('uuid', tank.uuid)
            tank.can_control = TankControl.CANT
            self.game_state.map.objects.add(tank)

        elif self.identifier == Identifier.ANIMATION:
            print('creating animation')
            bound = self.attributes.get('bound')
            times = self.attributes.get('times')
            next_frame_delay = self.attributes.get('next_frame_delay')

            animation_class = ManyTimesAnimation
            kwargs = {}

            if times:
                kwargs.update({'times': times})

            if next_frame_delay:
                kwargs.update({'next_frame_delay': next_frame_delay})

            if bound:
                animation_class = TankAttachedAnimation
                command = FindSpriteByUUID(uuid=bound, game_state=self.game_state)
                command.run()
                kwargs.update({'tank': command.result})

            animation = animation_class.create_from_code(
                code=self.object_identifier,
                start_y_cord=self.position_y,
                start_x_cord=self.position_x,
                game_state=self.game_state,
                **kwargs
            )

            self.game_state.map.objects.add(animation)
            print(animation)


class ParseAndApplyEvents(CommandContext):

    def __init__(self, dumped: str, game_state):
        self.dumped = dumped
        self.game_state = game_state

    def run(self) -> None:
        events = json.loads(self.dumped)

        def sorting_function(item: dict) -> bool:
            return item['type'] and item.get('identifier', 0)

        events = sorted(events, key=sorting_function)

        for event in events:
            event_command = {
                CustomEvents.REMOVE_OBJECT: ApplyRemoveEvent,
                CustomEvents.MOVE_OBJECT: ApplyMoveEvent,
                CustomEvents.CREATE_OBJECT: ApplyCreateEvent
            }[event.pop('type')]

            event_command(
                game_state=self.game_state, **event
            ).run()
