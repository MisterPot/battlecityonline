import pygame

from .enum import (
    DirectionCodes,
    Angles,
    TankCodes,
    BulletCodes,
    TileCodes,
    AnimationCodes
)
from .utils import (
    Environment,
    reverse_dict
)


ARROW_KEY_TO_DIRECTION = {
    pygame.K_DOWN: DirectionCodes.DOWN,
    pygame.K_LEFT: DirectionCodes.LEFT,
    pygame.K_RIGHT: DirectionCodes.RIGHT,
    pygame.K_UP: DirectionCodes.UP
}


DIRECTION_TO_ANGLE = {
    DirectionCodes.UP: Angles.UP,
    DirectionCodes.DOWN: Angles.DOWN,
    DirectionCodes.LEFT: Angles.LEFT,
    DirectionCodes.RIGHT: Angles.RIGHT
}


TANK_CODE_TO_RESOLVER = {
    TankCodes.SIMPLE_GOLD_TANK: Environment.TANKS.SIMPLE_GOLD_TANK
}
RESOLVER_TO_TANK_CODE = reverse_dict(TANK_CODE_TO_RESOLVER)


BULLET_CODE_TO_RESOLVER = {
    BulletCodes.BULLET: Environment.MISC.BULLET
}
RESOLVER_TO_BULLET_CODE = reverse_dict(BULLET_CODE_TO_RESOLVER)


TILE_CODE_TO_RESOLVER = {
    TileCodes.BRICK: Environment.TILES.BRICK_FULL,
    TileCodes.TREE: Environment.TILES.TREE,
    TileCodes.IRON: Environment.TILES.IRON,
    TileCodes.WATER_1: Environment.TILES.WATER_1,
    TileCodes.WATER_2: Environment.TILES.WATER_2,
    TileCodes.WATER_3: Environment.TILES.WATER_3,
    TileCodes.CONCRETE: Environment.TILES.CONCRETE
}
RESOLVER_TO_TILE_CODE = reverse_dict(TILE_CODE_TO_RESOLVER)


ANIMATION_CODE_TO_RESOLVERS = {
    AnimationCodes.SPAWN: (
        Environment.MISC.SPAWN_1,
        Environment.MISC.SPAWN_2,
        Environment.MISC.SPAWN_3,
        Environment.MISC.SPAWN_4
    ),
    AnimationCodes.PROTECTION: (
        Environment.MISC.PROTECTION_1,
        Environment.MISC.PROTECTION_2
    )
}
RESOLVERS_TO_ANIMATION_CODE = reverse_dict(ANIMATION_CODE_TO_RESOLVERS)
