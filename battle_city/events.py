from typing import Protocol, Dict, Any

import pygame

from .enum import CustomEvents


CUSTOM_EVENTS = [
    CustomEvents.MOVE_OBJECT,
    CustomEvents.REMOVE_OBJECT,
    CustomEvents.CREATE_OBJECT,
]


class CustomEvent(Protocol):
    type: int
    attributes: Dict[str, Any]


class CreateEvent(CustomEvent):
    identifier: int
    object_identifier: int
    position_x: int
    position_y: int


class MoveEvent(CustomEvent):
    from_x: int
    from_y: int
    to_x: int
    to_y: int


class RemoveEvent(CustomEvent):
    object_x: int
    object_y: int


def create_create_event(
        identifier: int,
        object_identifier: int,
        position_x: int,
        position_y: int,
        attributes: Dict[str, Any] = None
) -> CreateEvent:
    attributes = attributes or {}
    return pygame.event.Event(
        CustomEvents.CREATE_OBJECT,
        identifier=identifier,
        object_identifier=object_identifier,
        position_x=position_x,
        position_y=position_y,
        attributes=attributes
    )


def create_move_event(
        from_x: int,
        from_y: int,
        to_x: int,
        to_y: int,
        attributes: Dict[str, Any] = None
) -> MoveEvent:
    attributes = attributes or {}
    return pygame.event.Event(
        CustomEvents.MOVE_OBJECT,
        from_x=from_x,
        from_y=from_y,
        to_x=to_x,
        to_y=to_y,
        attributes=attributes
    )


def create_remove_event(
        object_x: int,
        object_y: int,
        attributes: Dict[str, Any] = None,
) -> RemoveEvent:
    attributes = attributes or {}
    return pygame.event.Event(
        CustomEvents.REMOVE_OBJECT,
        object_x=object_x,
        object_y=object_y,
        attributes=attributes
    )
