from typing import (
    Tuple,
    Union,
    List
)

import pygame

from .objects.game_object import (
    MovableObject,
    GameObject,
    Game
)
from .objects.tile import Tile
from .objects.utils import (
    get_ground_objects,
    get_movable_objects,
    get_passable_objects
)
from .enum import TileCodes
from .commands import (
    SpawnRingCommand,
    SpawnSquareCommand
)

SpriteCollection = Union[pygame.sprite.Group, List[GameObject]]
SpawnPlace = Tuple[int, int]


class TemporaryGroup:

    def __init__(self, *sprite_collections: SpriteCollection):
        self.sprite_collections = sprite_collections
        self.temporary_group: pygame.sprite.Group = None

    def __enter__(self):
        sprites = []

        for collection in self.sprite_collections:
            if isinstance(collection, pygame.sprite.Group):
                sprites.append(collection)
            else:
                sprites.extend(collection)

        self.temporary_group = pygame.sprite.Group(*sprites)
        return self.temporary_group

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.temporary_group.empty()


class GameMap:

    def __init__(self, game_state: Game):
        self.game_state = game_state
        self.screen = game_state.screen
        self.objects = pygame.sprite.Group()
        self.spawn_places: List[SpawnPlace] = []

    def dump_map(self) -> str:
        datas = []

        for sprite in self.objects.sprites():
            sprite: GameObject
            stringed = '!'.join(list(map(str, sprite.to_list())))
            datas.append(stringed)

        return '|'.join(datas)

    def init_map(self) -> None:
        ...

    def temporary_group(self) -> TemporaryGroup:
        return TemporaryGroup(
            self.ground_objects,
            self.movable_objects,
            self.passable_objects,
            self.objects
        )

    @property
    def movable_objects(self) -> List[MovableObject]:
        return get_movable_objects(self.objects.sprites())

    @property
    def ground_objects(self) -> List[Tile]:
        return get_ground_objects(self.objects.sprites())

    @property
    def passable_objects(self) -> List[GameObject]:
        return get_passable_objects(self.objects.sprites())

    def get_free_spawn_place(self) -> Union[SpawnPlace, None]:
        with self.temporary_group() as group:
            for spawn_place in self.spawn_places:
                ...

    def render_map(self) -> None:
        with self.temporary_group() as group:
            group.draw(self.screen)


class IronRingMap(GameMap):

    def init_map(self) -> None:
        SpawnRingCommand(
            start_x=0,
            start_y=0,
            size_x=13,
            size_y=13,
            game_state=self.game_state,
            tile_code=TileCodes.IRON
        ).run()


class SinglePlayerTestMap(IronRingMap):

    def init_map(self) -> None:
        super(SinglePlayerTestMap, self).init_map()
        SpawnSquareCommand(
            start_x=Tile.OBJECT_WIDTH,
            start_y=self.game_state.SCREEN_HEIGHT - Tile.OBJECT_HEIGHT * 5,
            size_y=4,
            size_x=4,
            game_state=self.game_state,
            tile_code=TileCodes.BRICK
        ).run()
        SpawnSquareCommand(
            start_x=432,
            start_y=432,
            size_y=3,
            size_x=3,
            game_state=self.game_state,
            tile_code=TileCodes.TREE
        ).run()
        SpawnSquareCommand(
            start_x=self.game_state.SCREEN_WIDTH - Tile.OBJECT_WIDTH * 5,
            start_y=Tile.OBJECT_HEIGHT,
            size_y=1,
            size_x=3,
            game_state=self.game_state,
            tile_code=TileCodes.WATER_1
        ).run()


class ServerTestMap(IronRingMap):

    def init_map(self) -> None:
        super(ServerTestMap, self).init_map()
        SpawnSquareCommand(
            start_x=self.game_state.SCREEN_WIDTH - Tile.OBJECT_WIDTH * 5,
            start_y=self.game_state.SCREEN_HEIGHT - Tile.OBJECT_HEIGHT * 5,
            size_y=4,
            size_x=4,
            game_state=self.game_state,
            tile_code=TileCodes.BRICK,
            spawn_function=Tile.create_particle_tile
        ).run()
        SpawnSquareCommand(
            start_x=Tile.OBJECT_WIDTH,
            start_y=Tile.OBJECT_HEIGHT * 4,
            size_x=4,
            size_y=4,
            game_state=self.game_state,
            tile_code=TileCodes.TREE
        ).run()
        SpawnSquareCommand(
            start_x=Tile.OBJECT_WIDTH,
            start_y=self.game_state.SCREEN_HEIGHT - Tile.OBJECT_HEIGHT * 2,
            size_x=4,
            size_y=1,
            game_state=self.game_state,
            tile_code=TileCodes.WATER_1,
        ).run()
