from __future__ import annotations
from typing import (
    Callable,
    List
)
import ctypes
from ctypes import wintypes

import pygame

from .enum import (
    TankCodes,
    DirectionCodes,
    Identifier,
    AnimationCodes
)
from .map_manager import (
    IronRingMap,
    SinglePlayerTestMap
)
from .commands import ParseObjectCommand
import battle_city.objects.tank as tank

EventHandler = Callable[[pygame.event.Event], bool]
EventHandlers = List[EventHandler]
EVENT_HANDLERS: EventHandlers = []


def add_handler(
        handlers: EventHandlers,
        preferable_event_index: int = -1
) -> Callable[[EventHandler], EventHandler]:
    def function_wrapper(function: EventHandler) -> None:
        if preferable_event_index == -1:
            handlers.append(function)
        else:
            handlers.insert(preferable_event_index, function)

    return function_wrapper


class GameContext:
    SCREEN_WIDTH = 624
    SCREEN_HEIGHT = 624

    def __init__(self):
        self.running = False
        self.screen = pygame.display.set_mode(
            size=(self.SCREEN_WIDTH, self.SCREEN_HEIGHT)
        )
        hwnd = pygame.display.get_wm_info()['window']
        user32 = ctypes.WinDLL('user32')
        user32.SetWindowPos.restype = wintypes.HWND
        user32.SetWindowPos.argtypes = [
            wintypes.HWND,
            wintypes.HWND,
            wintypes.INT,
            wintypes.INT,
            wintypes.INT,
            wintypes.INT,
            wintypes.UINT
        ]
        user32.SetWindowPos(hwnd, -1, 600, 300, 0, 0, 0x0001)

        self.clock = pygame.time.Clock()
        self.init_map()

    def init_map(self) -> None:
        self.map = IronRingMap(game_state=self)

    def render(self, screen) -> None:
        screen.fill(color=(0, 0, 0))
        with self.map.temporary_group() as temporary_group:
            temporary_group.draw(surface=screen)
        pygame.display.flip()


class SingleGame(GameContext):

    def init_map(self) -> None:
        self.map = SinglePlayerTestMap(game_state=self)
        self.tank = tank.Tank.create_from_code(
            code=TankCodes.SIMPLE_GOLD_TANK,
            start_x_cord=300,
            start_y_cord=300,
            game_state=self,
        )
        spawn = tank.SpawnTankAnimation.create_from_code(
            code=AnimationCodes.SPAWN,
            start_x_cord=100,
            start_y_cord=100,
            game_state=self,
            tank=self.tank
        )
        self.map.objects.add(spawn)
        self.map.init_map()

    @add_handler(handlers=EVENT_HANDLERS, preferable_event_index=3)
    def create_another_tank(self, event: pygame.event.Event) -> bool:
        if event.type == pygame.KEYDOWN and event.key == pygame.K_TAB:
            command = ParseObjectCommand(
                data_list=f"{7}!{Identifier.TANK}!{TankCodes.SIMPLE_GOLD_TANK}!{200}!{200}!{1}!{DirectionCodes.DOWN}",
                storage=[],
                game_state=self
            )
            command.print_stack()
            command.run()
        return False

    @add_handler(handlers=EVENT_HANDLERS, preferable_event_index=2)
    def control_another_tank(self, event: pygame.event.Event) -> bool:
        if event.type == pygame.KEYDOWN and event.key in [
            pygame.K_w, pygame.K_a, pygame.K_d, pygame.K_s
        ]:
            ...
            # direction = {
            #     pygame.K_w: DirectionCodes.UP,
            #     pygame.K_s: DirectionCodes.DOWN,
            #     pygame.K_a: DirectionCodes.LEFT,
            #     pygame.K_d: DirectionCodes.RIGHT
            # }[event.key]
            # speed = tank.Tank.get_speed_values(speed=5)[direction]
            # self.another_tank.move(distance=speed, direction=direction)
        return False

    @add_handler(handlers=EVENT_HANDLERS, preferable_event_index=0)
    def exit(self, event: pygame.event.Event) -> bool:
        if event.type == pygame.QUIT:
            self.running = False
            return True
        return False

    @add_handler(handlers=EVENT_HANDLERS, preferable_event_index=1)
    def exit_on_key(self, event: pygame.event.Event) -> bool:
        if event.type == pygame.KEYDOWN and event.key == 27:
            self.running = False
            return True
        return False

    @staticmethod
    def handle_events(game: GameContext, handlers: EventHandlers) -> None:
        break_loop = False

        for event in pygame.event.get():
            for handler in handlers:
                break_loop = handler(game, event)

                if break_loop:
                    break

            if break_loop:
                break

    def run(self) -> None:
        pygame.init()

        self.running = True

        while self.running:
            self.handle_events(game=self, handlers=EVENT_HANDLERS)
            self.render(screen=self.screen)
            self.map.objects.update()
            self.clock.tick(60)
