from typing import Tuple

import pygame.image

from .game_object import GameObject
from ..converters import (
    ANIMATION_CODE_TO_RESOLVERS,
    RESOLVERS_TO_ANIMATION_CODE,
)
from ..utils import PathResolver
from ..enum import Identifier


class Animation(GameObject):

    IDENTIFIER = Identifier.ANIMATION

    TO_CODE = RESOLVERS_TO_ANIMATION_CODE
    TO_PATH = ANIMATION_CODE_TO_RESOLVERS

    OBJECT_WIDTH = 39
    OBJECT_HEIGHT = 39

    def __init__(self, next_frame_delay: int = 2000, *args, **kwargs):
        kwargs.update({'auto_init': False})
        super(Animation, self).__init__(*args, **kwargs)

        self._image_path: Tuple[PathResolver]
        self.frames = [
            self.autoscale(pygame.image.load(resolver.string()))
            for resolver in self._image_path
        ]
        self.time_per_update = self.game_state.clock.get_fps() or 60
        self.next_frame_delay = next_frame_delay
        self.current_frame_delay = 0
        self.current_frame_index = 0
        self.image = self.frames[0]
        self.playing = True

        self.init_rect()
        self.init_mask()

    def update_starts(self) -> None:

        self.current_frame_delay += self.time_per_update

        if self.next_frame_delay <= self.current_frame_delay:
            self.current_frame_index += 1
            self.current_frame_delay = 0

    def update(self) -> None:
        ...

    def update_ends(self) -> None:

        current_frame = self.frames[self.current_frame_index]
        self.image = self.autoscale(image=current_frame)

        self.init_rect()
        self.init_mask()

    def animation_ending(self) -> None:
        self.kill()


class ManyTimesAnimation(Animation):

    def __init__(self, times: int = 1, *args, **kwargs):
        super(ManyTimesAnimation, self).__init__(*args, **kwargs)
        self.times = times
        self.current_times = 0

    def update(self) -> None:

        if not self.playing:
            self.animation_ending()
            return

        self.update_starts()

        if self.times == self.current_times:
            self.playing = False
            self.animation_ending()
            return

        if self.current_frame_index >= len(self.frames):
            self.current_frame_delay = 0
            self.current_frame_index = 0
            self.current_times += 1

        self.update_ends()


class EndlessAnimation(Animation):

    def update(self) -> None:

        if not self.playing:
            self.animation_ending()
            return

        self.update_starts()

        if self.current_frame_index >= len(self.frames):
            self.current_frame_delay = 0
            self.current_frame_index = 0

        self.update_ends()
