from __future__ import annotations


import pygame.sprite

from .game_object import (
    GameObject,
    Game
)
from ..converters import (
    TILE_CODE_TO_RESOLVER,
    RESOLVER_TO_TILE_CODE
)
from ..enum import Identifier


class Tile(GameObject):

    IDENTIFIER = Identifier.TILE

    TO_PATH = TILE_CODE_TO_RESOLVER
    TO_CODE = RESOLVER_TO_TILE_CODE

    OBJECT_WIDTH = 48
    OBJECT_HEIGHT = 48

    @classmethod
    def create_tile_part(
            cls,
            game_state: Game,
            code: int,
            start_x_cord: int = None,
            start_y_cord: int = None,
    ):
        brick_height = Tile.OBJECT_HEIGHT // 2
        brick_width = Tile.OBJECT_WIDTH // 2
        tile = cls.create_from_code(
            code=code,
            game_state=game_state,
            start_x_cord=start_x_cord,
            start_y_cord=start_y_cord
        )
        tile.OBJECT_WIDTH = brick_width
        tile.OBJECT_HEIGHT = brick_height

        tile.init_image()
        tile.init_rect()
        tile.init_mask()

        return tile

    @classmethod
    def create_particle_tile(
            cls,
            game_state: Game,
            code: int,
            start_x_cord: int = None,
            start_y_cord: int = None,
    ) -> pygame.sprite.Group:

        brick_height = Tile.OBJECT_HEIGHT // 2
        brick_width = Tile.OBJECT_WIDTH // 2
        brick_group = pygame.sprite.Group()
        current_x = start_x_cord
        current_y = start_y_cord

        for _ in range(2):
            for _ in range(2):

                tile = cls.create_tile_part(
                    game_state=game_state,
                    start_y_cord=current_y,
                    start_x_cord=current_x,
                    code=code,
                )

                tile.init_image()
                tile.init_rect()
                tile.init_mask()
                brick_group.add(tile)

                current_x += brick_width

            current_y += brick_height
            current_x = start_x_cord

        return brick_group
