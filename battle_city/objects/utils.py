from typing import (
    List,
    Tuple,
    Callable,
    TypeVar
)

from ..enum import (
    TileCodes,
    Identifier,
    BulletCodes,
    TankCodes,
    AnimationCodes
)
from .game_object import GameObject


GROUND_TILES = [
    (Identifier.TILE, TileCodes.WATER_1),
    (Identifier.TILE, TileCodes.WATER_2),
    (Identifier.TILE, TileCodes.WATER_3),
    (Identifier.TILE, TileCodes.CONCRETE),
]

MOVABLE_OBJECTS = [
    (Identifier.TANK, TankCodes.SIMPLE_GOLD_TANK),
    (Identifier.ANIMATION, AnimationCodes.PROTECTION)
]

KILLABLE_OBJECTS = [
    (Identifier.BULLET, BulletCodes.BULLET),
    (Identifier.TILE, TileCodes.BRICK),
]

PASSABLE_OBJECTS = [
    (Identifier.TILE, TileCodes.TREE),
    (Identifier.ANIMATION, AnimationCodes.PROTECTION)
]


_V = TypeVar('_V')


def check_identifier(obj: GameObject, identifier: int) -> bool:
    return obj.IDENTIFIER == identifier


def check_code(obj: GameObject, code: int) -> bool:
    return obj._code == code


def is_tile(collision: GameObject) -> bool:
    return collision.IDENTIFIER == Identifier.TILE


def use_filter(object_filter: List[_V]) -> Callable[[GameObject, _V], Callable[[GameObject], bool]]:
    def function_wrapper(function: Callable[[GameObject, _V], bool]) -> Callable[[GameObject], bool]:
        def args_wrapper(obj: GameObject) -> bool:
            for filter_item in object_filter:
                if function(obj, filter_item):
                    return True
            return False
        return args_wrapper
    return function_wrapper


def objects_of(filter_function: Callable[[GameObject], bool]) -> Callable[[List[GameObject]], List[GameObject]]:
    def function_wrapper(object_list: List[GameObject]) -> List[GameObject]:
        valid_objects = []
        for obj in object_list:
            if filter_function(obj):
                valid_objects.append(obj)
        return valid_objects
    return function_wrapper


def check_indent_and_code(obj: GameObject, filtering_item: Tuple[int, int]) -> bool:
    identifier, code = filtering_item
    checked_ident = check_identifier(obj=obj, identifier=identifier)
    checked_code = check_code(obj=obj, code=code)
    return checked_ident and checked_code


is_passable = use_filter(object_filter=PASSABLE_OBJECTS)(check_indent_and_code)
is_killable = use_filter(object_filter=KILLABLE_OBJECTS)(check_indent_and_code)
is_movable = use_filter(object_filter=MOVABLE_OBJECTS)(check_indent_and_code)
is_ground_tile = use_filter(object_filter=GROUND_TILES)(check_indent_and_code)


get_killable_objects = objects_of(filter_function=is_killable)
get_ground_objects = objects_of(filter_function=is_ground_tile)
get_movable_objects = objects_of(filter_function=is_movable)
get_passable_objects = objects_of(filter_function=is_passable)
