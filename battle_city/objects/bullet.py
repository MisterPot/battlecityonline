from typing import Callable

import pygame.transform

from ..enum import (
    Identifier,
    DirectionCodes,
)
from ..converters import (
    BULLET_CODE_TO_RESOLVER,
    RESOLVER_TO_BULLET_CODE,
    DIRECTION_TO_ANGLE
)
from ..events import (
    create_move_event,
    create_remove_event
)
from .utils import (
    is_tile,
    get_killable_objects,
    is_ground_tile
)
from .game_object import MovableObject


DirectionFunction = Callable[[int], None]


class Bullet(MovableObject):

    IDENTIFIER = Identifier.BULLET

    TO_PATH = BULLET_CODE_TO_RESOLVER
    TO_CODE = RESOLVER_TO_BULLET_CODE

    OBJECT_WIDTH = 12
    OBJECT_HEIGHT = 16

    def post_init(self) -> None:
        super(Bullet, self).post_init()
        self.image = pygame.transform.rotate(
            surface=self.image,
            angle=DIRECTION_TO_ANGLE[self.DIRECTION]
        )

    def get_direction_function(self) -> DirectionFunction:
        return {
            DirectionCodes.UP: self.move_y,
            DirectionCodes.DOWN: self.move_y,
            DirectionCodes.LEFT: self.move_x,
            DirectionCodes.RIGHT: self.move_x
        }[self.DIRECTION]

    def update(self, speed: int = 10) -> None:

        direction_function = self.get_direction_function()
        to_move = self.get_speed_values(speed=speed)[self.DIRECTION]
        collisions = self.future_collisions(distance=to_move, direction=self.DIRECTION)

        if collisions:

            if not (
                any(map(is_tile, collisions)) and
                all(map(is_ground_tile, collisions))
            ):
                self.kill()
                event = create_remove_event(
                    object_x=self.rect.x,
                    object_y=self.rect.y,
                    attributes={'uuid': self.uuid}
                )
                pygame.event.post(event)

            killable_collisions = get_killable_objects(collisions)
            if killable_collisions:
                for collision in killable_collisions:
                    collision.kill()
                    kill_event = create_remove_event(
                        object_x=collision.rect.x,
                        object_y=collision.rect.y
                    )
                    pygame.event.post(kill_event)

        last_x, last_y = self.rect.topleft
        direction_function(to_move)
        self.rect.clamp_ip(self.game_state.screen.get_rect())
        move_event = create_move_event(
            from_x=last_x,
            from_y=last_y,
            to_y=self.rect.y,
            to_x=self.rect.x,
            attributes={'DIRECTION': self.DIRECTION, 'uuid': self.uuid}
        )
        pygame.event.post(move_event)
