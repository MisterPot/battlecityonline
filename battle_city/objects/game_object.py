from __future__ import annotations

import uuid
from typing import (
    Tuple,
    Union,
    Dict,
    List,
    Protocol,
    Generic
)

import pygame
from pygame.sprite import Sprite

from ..utils import (
    PathResolver,
    expect_difference
)
from ..enum import (
    DirectionCodes,
    Angles
)
from ..converters import DIRECTION_TO_ANGLE


class Game(Protocol):
    running: bool
    screen: pygame.surface.Surface
    clock: pygame.time.Clock




class GameObject(Sprite):

    IDENTIFIER = None

    TO_CODE = None
    TO_PATH = None

    OBJECT_HEIGHT = None
    OBJECT_WIDTH = None

    def __init__(
            self,
            sprite_image_path: PathResolver,
            game_state: Game,
            start_x_cord: int = None,
            start_y_cord: int = None,
            mask_size_multiplier: float = 1.02,
            auto_init: bool = True
    ):
        Sprite.__init__(self)
        self.game_state = game_state
        self._image_path = sprite_image_path
        self._code = self.TO_CODE[sprite_image_path]
        self.start_x_cord = start_x_cord
        self.start_y_cord = start_y_cord
        self.multiplier = mask_size_multiplier

        if auto_init:
            self.init_image()
            self.init_rect()
            self.init_mask()
        self.post_init()

    def autoscale(self, image: pygame.surface.Surface) -> pygame.surface.Surface:
        return pygame.transform.scale(
            surface=image,
            size=(self.OBJECT_WIDTH, self.OBJECT_HEIGHT)
        )

    def init_image(self) -> None:
        image = pygame.image.load(self._image_path.string())
        self.image = self.autoscale(image=image)

    def init_rect(self) -> None:
        self.rect = self.image.get_rect()
        self.rect.topleft = (self.start_x_cord, self.start_y_cord)

    def init_mask(self) -> None:
        size_for_mask = (
            self.rect.width * self.multiplier,
            self.rect.height * self.multiplier
        )
        self.mask = pygame.mask.from_surface(
            surface=pygame.transform.scale(
                surface=self.image,
                size=size_for_mask
            )
        )

    def post_init(self) -> None:
        ...

    @classmethod
    def get_image_resolver(cls, code: int) -> PathResolver:
        return cls.TO_PATH[code]

    @classmethod
    def create_from_code(
            cls,
            code: int,
            game_state: Game,
            start_x_cord: int = None,
            start_y_cord: int = None,
            **kwargs
    ) -> Generic[GameObject]:
        return cls(
            sprite_image_path=cls.get_image_resolver(code=code),
            start_x_cord=start_x_cord,
            start_y_cord=start_y_cord,
            game_state=game_state,
            **kwargs
        )

    def cords(self) -> Tuple[int, int]:
        return self.rect.topleft

    def additional_array_data(self) -> List[int]:
        return []

    def to_list(self) -> List[int]:
        data = [
            self.IDENTIFIER,
            self._code,
            self.rect.x,
            self.rect.y,
            int(self.alive())
        ]
        data.extend(self.additional_array_data())
        data.insert(0, len(data))
        return data

    def get_collisions(self) -> List[GameObject]:
        collisions = pygame.sprite.spritecollide(
            sprite=self,
            group=self.map,
            collided=pygame.sprite.collide_mask,
            dokill=False
        )
        filter_function = lambda sprite: sprite != self
        return list(filter(filter_function, collisions))

    @property
    def map(self) -> Union[pygame.sprite.Group, None]:
        return self.game_state.map.objects


class MovableObject(GameObject):

    DIRECTION = DirectionCodes.UP
    ANGLE = Angles.UP
    ROTATING = 90

    def post_init(self) -> None:
        super(MovableObject, self).post_init()
        self.uuid = uuid.uuid4().int

    def additional_array_data(self) -> List[int]:
        return [self.DIRECTION, self.uuid]

    @staticmethod
    def can_modify_x(direction: int) -> bool:
        return direction in [DirectionCodes.LEFT, DirectionCodes.RIGHT]

    @staticmethod
    def can_modify_y(direction: int) -> bool:
        return direction in [DirectionCodes.UP, DirectionCodes.DOWN]

    def future_collisions(
            self,
            direction: int,
            distance: int = 2,
    ) -> GameObject:
        x_modified = y_modified = False

        if self.can_modify_y(direction=direction):
            self.move_y(to_move=distance)
            y_modified = True
        elif self.can_modify_x(direction=direction):
            self.move_x(to_move=distance)
            x_modified = True

        collision = self.get_collisions()

        if x_modified:
            self.move_x(to_move=-distance)
        elif y_modified:
            self.move_y(to_move=-distance)

        return collision

    def rotate(self, direction: int) -> None:

        if self.DIRECTION == direction:
            return

        to_rotate = expect_difference(
            input_number=self.ANGLE,
            operable_number=self.ROTATING,
            expected=DIRECTION_TO_ANGLE[direction]
        )
        self.image = pygame.transform.rotate(
            surface=self.image,
            angle=to_rotate
        )
        self.ANGLE += to_rotate
        self.DIRECTION = direction

    def move_x(self, to_move: int) -> None:
        self.rect.x += to_move

    def move_y(self, to_move: int) -> None:
        self.rect.y += to_move

    @staticmethod
    def get_pressed_directions(pressed: dict) -> Dict[int, bool]:
        return {
            DirectionCodes.LEFT: pressed[pygame.K_LEFT],
            DirectionCodes.RIGHT: pressed[pygame.K_RIGHT],
            DirectionCodes.UP: pressed[pygame.K_UP],
            DirectionCodes.DOWN: pressed[pygame.K_DOWN]
        }

    @staticmethod
    def get_speed_values(speed: int = 1) -> Dict[int, int]:
        return {
            DirectionCodes.UP: -speed,
            DirectionCodes.DOWN: speed,
            DirectionCodes.LEFT: -speed,
            DirectionCodes.RIGHT: speed,
        }
