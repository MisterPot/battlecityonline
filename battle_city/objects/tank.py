from __future__ import annotations

import threading
from typing import Dict, Union, Tuple

import pygame.event

from ..converters import (
    TANK_CODE_TO_RESOLVER,
    RESOLVER_TO_TANK_CODE
)
from ..enum import (
    TankControl,
    Identifier,
    BulletCodes,
    DirectionCodes,
    AnimationCodes,
)
from .animation import ManyTimesAnimation
from ..events import (
    create_create_event,
    create_move_event,
)
from .utils import is_passable
from .game_object import MovableObject
from .bullet import Bullet


class TankAnimation(ManyTimesAnimation):

    def __init__(self, tank: Tank, **kwargs):
        self.tank = tank
        super(TankAnimation, self).__init__(**kwargs)


class TankAttachedAnimation(TankAnimation):

    def __init__(self, **kwargs):
        super(TankAttachedAnimation, self).__init__(**kwargs)
        self.OBJECT_WIDTH += 14
        self.OBJECT_HEIGHT += 14

    def init_rect(self) -> None:
        new_rect = self.tank.rect.copy()
        new_rect.x -= 7
        new_rect.y -= 7
        new_rect.width = self.tank.OBJECT_WIDTH + 7
        new_rect.height += self.tank.OBJECT_HEIGHT + 7
        self.rect = new_rect


class SpawnTankAnimation(TankAnimation):

    def __init__(self, **kwargs):
        super(SpawnTankAnimation, self).__init__(**kwargs)
        self.events = []
        self.spawned = threading.Event()

    def animation_ending(self) -> None:
        self.kill()
        self.tank.rect = self.rect

        protection_time = 30
        next_frame_delay = 500

        protection_animation = TankAttachedAnimation.create_from_code(
            code=AnimationCodes.PROTECTION,
            start_x_cord=self.tank.rect.x,
            start_y_cord=self.tank.rect.y,
            game_state=self.tank.game_state,
            tank=self.tank,
            next_frame_delay=next_frame_delay,
            times=protection_time
        )

        self.events.extend([
            create_create_event(
                identifier=Identifier.TANK,
                object_identifier=self.tank._code,
                position_x=self.tank.rect.x,
                position_y=self.tank.rect.y,
                attributes={
                    'DIRECTION': self.tank.DIRECTION,
                    'uuid': self.tank.uuid
                }
            ),
            create_create_event(
                identifier=Identifier.ANIMATION,
                object_identifier=AnimationCodes.PROTECTION,
                position_x=protection_animation.rect.x,
                position_y=protection_animation.rect.y,
                attributes={
                    'bound': self.tank.uuid,
                    'times': protection_time,
                    'next_frame_delay': next_frame_delay
                }
            ),
        ])

        self.game_state.map.objects.add(self.tank)
        self.game_state.map.objects.add(protection_animation)
        self.spawned.set()


class Tank(MovableObject):

    IDENTIFIER = Identifier.TANK

    TO_PATH = TANK_CODE_TO_RESOLVER
    TO_CODE = RESOLVER_TO_TANK_CODE

    OBJECT_WIDTH = 39
    OBJECT_HEIGHT = 39

    COOLDOWN = 0
    MAX_COOLDOWN = 400

    def post_init(self) -> None:
        super(Tank, self).post_init()
        self.can_control = TankControl.CAN

    @staticmethod
    def get_one_pressed(pressed: Dict[int, int]) -> Union[Tuple[int, int], None]:
        directions = Tank.get_pressed_directions(pressed=pressed)
        key = list(filter(
            lambda item: item[1],
            directions.items()
        ))
        return key

    def decrease_cooldown(self) -> None:
        calculated_cooldown = self.COOLDOWN - self.game_state.clock.get_time()
        self.COOLDOWN = calculated_cooldown if calculated_cooldown >= 0 else 0

    def update(self, speed: int = 5, pressed_keys: dict = None) -> None:

        if self.can_control == TankControl.CANT:
            return

        pressed = pressed_keys or pygame.key.get_pressed()
        self.decrease_cooldown()
        key = self.get_one_pressed(pressed=pressed)
        self.perform_move(key=key, speed=speed)

        if pressed[pygame.K_SPACE]:
            self.spawn_bullet()

    def perform_move(self, key: Union[int, None], speed: int) -> None:

        if not key:
            return

        direction = key[0][0]
        pygame.key.set_repeat(1, speed)
        speed_values = self.get_speed_values(speed=speed)
        current_speed = speed_values[direction]

        self.move(distance=current_speed, direction=direction)

    def move(self, distance: int, direction: int) -> None:
        future_collisions = self.future_collisions(
            distance=distance, direction=direction
        )

        if future_collisions and not all(map(is_passable, future_collisions)):
            return

        last_x, last_y = self.rect.topleft

        if self.can_modify_y(direction=direction):
            self.move_y(to_move=distance)

        elif self.can_modify_x(direction=direction):
            self.move_x(to_move=distance)

        self.rotate(direction=direction)
        self.rect.clamp_ip(self.game_state.screen.get_rect())
        event = create_move_event(
            from_x=last_x,
            from_y=last_y,
            to_x=self.rect.x,
            to_y=self.rect.y,
            attributes={'DIRECTION': self.DIRECTION, 'uuid': self.uuid}
        )
        pygame.event.post(event)

    def get_bullet_start_cords(self) -> Dict:
        distance = Bullet.OBJECT_WIDTH - 5
        bullet_middle = Bullet.OBJECT_WIDTH / 2

        return {
            DirectionCodes.LEFT: (
                self.rect.midleft[0] - distance,
                self.rect.midleft[1] - bullet_middle
            ),
            DirectionCodes.RIGHT: (
                self.rect.midright[0] + distance,
                self.rect.midright[1] - bullet_middle
            ),
            DirectionCodes.UP: (
                self.rect.midtop[0] - bullet_middle,
                self.rect.midtop[1] - distance*1.5
            ),
            DirectionCodes.DOWN: (
                self.rect.midbottom[0] - bullet_middle,
                self.rect.midbottom[1] + distance
            )
        }[self.DIRECTION]

    def spawn_bullet(self) -> None:

        if self.COOLDOWN != 0:
            return

        self.COOLDOWN = self.MAX_COOLDOWN
        start_x, start_y = self.get_bullet_start_cords()
        bullet = Bullet.create_from_code(
            code=BulletCodes.BULLET,
            start_x_cord=start_x,
            start_y_cord=start_y,
            game_state=self.game_state,
        )

        bullet.rotate(self.DIRECTION)
        pygame.event.post(
            create_create_event(
                identifier=Identifier.BULLET,
                object_identifier=BulletCodes.BULLET,
                position_x=start_x,
                position_y=start_y,
                attributes={
                    'DIRECTION': self.DIRECTION,
                    'uuid': bullet.uuid
                }
            )
        )
        self.game_state.map.objects.add(bullet)
