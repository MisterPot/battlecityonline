import errno
import json
import socket
import sys
import threading
import time
from typing import Callable

import pygame

from ..commands import (
    ParseClientDataCommand,
    ParseAndApplyEvents
)
from ..game import GameContext
from .session import (
    ClientSession,
    ClientNetworkManager
)
from ..map_manager import GameMap


class ClientGame(GameContext):

    def __init__(
            self,
            host: str,
            target_host: str,
            target_tcp_port: int,
            manager_receive_function: Callable[[socket.socket], bytes] = None,
            **kwargs
    ):
        self.host = host
        self.target_host = target_host
        self.target_tcp_port = target_tcp_port
        self.manager_receive_function = manager_receive_function or ClientNetworkManager.receive_tcp_message_full
        self.frames_per_second = None
        super(ClientGame, self).__init__(**kwargs)

    def init_map(self) -> None:
        manager = ClientNetworkManager(
            host=self.host,
            target_host=self.target_host,
            target_tcp_port=self.target_tcp_port,
            receive_function=self.manager_receive_function
        )
        self.session = ClientSession(manager=manager)
        pygame.display.set_caption('Battle City Reloaded')
        self.map = GameMap(game_state=self)

    def pinging(self) -> threading.Event:

        stop_ping = threading.Event()

        def wrapper() -> None:

            while not stop_ping.is_set():

                try:
                    stop_ping.wait(timeout=20)
                    self.session.ping()
                except OSError as e:
                    if e.errno != errno.ENOTSOCK:
                        raise e

        thread = threading.Thread(target=wrapper)
        thread.start()

        return stop_ping

    def run(self) -> None:
        pygame.init()

        self.running = True

        # authorize connection
        self.session.ask_uuid()

        if not self.session.uuid:
            sys.exit()

        # sending udp server address to server via tcp connection
        self.session.ask_udp_server()

        # receiving dumped map data from server
        self.session.ask_map_data()
        init_data = self.session.map_data

        # and receiving frame rate for game
        self.session.ask_frame_rate()
        self.frames_per_second = int(self.session.frame_rate)

        pygame.key.set_repeat(1, 5)
        stop_ping = self.pinging()
        print(init_data)

        ParseClientDataCommand(
            data_string=init_data,
            game_state=self
        ).run()

        while self.running:
            try:
                for _ in pygame.event.get():
                    pass

                pressed = pygame.key.get_pressed()
                pressed_keys = {
                    pygame.K_DOWN: int(pressed[pygame.K_DOWN]),
                    pygame.K_LEFT: int(pressed[pygame.K_LEFT]),
                    pygame.K_RIGHT: int(pressed[pygame.K_RIGHT]),
                    pygame.K_UP: int(pressed[pygame.K_UP]),
                    pygame.K_SPACE: int(pressed[pygame.K_SPACE])
                }

                if any(pressed_keys.values()):
                    self.session.manager.send_udp_message_attached(
                        sending_socket=self.session.udp_connection,
                        message=json.dumps(pressed_keys)
                    )

                events = self.session.receive_events_udp_not_blocking()
                if events and not isinstance(json.loads(events), int):
                    ParseAndApplyEvents(
                        dumped=events,
                        game_state=self
                    ).run()

                tcp_events = self.session.receive_events_tcp_not_blocking()

                if tcp_events and not isinstance(json.loads(tcp_events), int):
                    ParseAndApplyEvents(
                        dumped=tcp_events,
                        game_state=self
                    ).run()

                self.render(screen=self.screen)
                self.map.objects.update()
                self.clock.tick(self.frames_per_second)

            except KeyboardInterrupt:
                stop_ping.set()
                self.running = False
                self.session.manager.tcp_connection.close()
