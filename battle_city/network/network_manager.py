import errno
import socket
import threading
import time
from abc import abstractmethod, ABC
from typing import Union, Callable, Tuple

Address = Tuple[str, int]


class NetworkManager(ABC):

    def __init__(
            self,
            header_size: int = 10,
            encoding: str = None,
            receive_function: Callable[[socket.socket], bytes] = None
    ):
        self.header_size = header_size
        self.encoding = encoding or 'utf-8'
        self.receive_function = receive_function or NetworkManager.receive_tcp_message_full

        self.tcp_connection = socket.socket(
            family=socket.AF_INET,
            type=socket.SOCK_STREAM
        )

    def receive_tcp_message(self, receiving_socket: socket.socket, **kwargs) -> bytes:
        return self.receive_function(self, receiving_socket=receiving_socket, **kwargs)

    @abstractmethod
    def init_connection(self) -> None:
        ...

    @staticmethod
    def create_udp_server(binding_host: str, ttl: int = 40) -> Tuple[socket.socket, Address]:
        temp_sock = socket.socket(
            family=socket.AF_INET,
            type=socket.SOCK_DGRAM
        )
        temp_sock.bind((binding_host, 0))
        address = temp_sock.getsockname()
        temp_sock.close()
        server = socket.socket(
            family=socket.AF_INET,
            type=socket.SOCK_DGRAM
        )
        server.bind(address)
        server.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
        return server, address

    def message_header(self, message: str) -> bytes:
        return f'{len(message):<{self.header_size}}'.encode(self.encoding)

    def tcp_packet(self, message: str) -> bytes:
        header = self.message_header(message=message)
        return header + message.encode(self.encoding)

    def send_tcp_message(self, message: str) -> None:
        self.tcp_connection.sendall(self.tcp_packet(message=message))

    def receive_tcp_message_full(self, receiving_socket: socket.socket) -> Union[None, bytes]:
        message_size = self.get_next_tcp_message_size(
            receiving_socket=receiving_socket
        )
        if message_size:
            return receiving_socket.recv(message_size)

    def send_udp_message_not_attached(
            self,
            sending_socket: socket.socket,
            message: str,
            address: Address
    ) -> None:
        message_size = self.message_header(message=message)
        sending_socket.sendto(message_size, address)
        sending_socket.sendto(message.encode(self.encoding), address)

    def send_udp_message_attached(
            self,
            sending_socket: socket.socket,
            message: str
    ):
        message_header = self.message_header(message=message)
        sending_socket.sendall(message_header)
        sending_socket.sendall(message.encode(self.encoding))

    def receive_udp_message_full_attached(
            self,
            receiving_socket: socket.socket
    ) -> Union[bytes, None]:
        try:
            message_size = receiving_socket.recv(self.header_size).decode(self.encoding)
            return receiving_socket.recv(int(message_size))
        except OSError as error:
            if error.errno != errno.EMSGSIZE:
                raise error

    def receive_udp_message_full_not_attached(
            self,
            receiving_socket: socket.socket,
    ) -> Union[Tuple[bytes, Address], None]:
        try:
            message_header, address = receiving_socket.recvfrom(self.header_size)
            return receiving_socket.recvfrom(int(message_header))
        except OSError as error:
            if error.errno != errno.EMSGSIZE:
                raise error

    def get_next_tcp_message_size(self, receiving_socket: socket.socket) -> Union[None, int]:
        try:
            header = receiving_socket.recv(self.header_size)
            if not header:
                return
            return int(header.decode(self.encoding).strip())
        except:
            pass

    def receive_tcp_message_buffered(
            self,
            receiving_socket: socket.socket,
            buffer_size: int = 256
    ) -> Union[None, bytes]:
        message_size = self.get_next_tcp_message_size(
            receiving_socket=receiving_socket
        )

        if not message_size:
            return

        bytes_received = 0
        full_message = b''

        while bytes_received != message_size:
            will_be_received = bytes_received + buffer_size
            overflowed = buffer_size - (will_be_received - message_size)
            bytes_to_receive = buffer_size if will_be_received <= message_size else overflowed
            full_message += receiving_socket.recv(bytes_to_receive)
            bytes_received += bytes_to_receive

        return full_message


class ClientNetworkManager(NetworkManager):

    def __init__(
            self,
            host: str,
            target_host: str,
            target_tcp_port: int,
            **kwargs
    ):
        self.host = host
        self.target_tcp_port = target_tcp_port
        self.target_host = target_host
        super(ClientNetworkManager, self).__init__(**kwargs)
        self.init_connection()

    def init_connection(self) -> None:
        self.tcp_connection.connect((self.target_host, self.target_tcp_port))

    def test_run(self) -> None:

        udp_server, udp_address = self.create_udp_server(binding_host=self.host)
        host, address = udp_address
        print(f'Created udp server - {udp_address} ...')

        self.send_tcp_message(f'{host}:{address}')
        print('Sent udp address to main server ...')

        server_udp_address = self.receive_tcp_message(
            receiving_socket=self.tcp_connection
        ).decode(self.encoding)
        print(f'Received main server udp address - {server_udp_address} ...')

        server_host, server_port = server_udp_address.split(':')
        udp_server.connect((server_host, int(server_port)))
        print('Connected to main udp server ...')

        while True:
            message = self.receive_udp_message_full_attached(
                receiving_socket=udp_server
            )
            print(message)


class ServerNetworkManager(NetworkManager):

    def __init__(
            self,
            host: str,
            tcp_port: int,
            **kwargs
    ):
        self.host = host
        self.tcp_port = tcp_port
        super(ServerNetworkManager, self).__init__(**kwargs)
        self.init_connection()

    def init_connection(self) -> None:
        self.tcp_connection.bind((self.host, self.tcp_port))
        self.tcp_connection.listen()

    def test_run(self) -> None:
        while True:
            print('Wait for connection')
            client_socket, address = self.tcp_connection.accept()
            threading.Thread(
                target=self.start_udp_server,
                args=(client_socket, address)
            ).start()

    def start_udp_server(self, client_tcp_connection: socket.socket, address: Address) -> None:

        udp_address = self.receive_tcp_message_buffered(
            receiving_socket=client_tcp_connection
        ).decode(self.encoding)
        print(f'Connection established with - {address}')
        print(f'Received client server udp - {udp_address}')

        client_host, client_port = udp_address.split(':')
        udp_client_server, server_address = self.create_udp_server(binding_host=self.host)
        udp_client_server.connect((client_host, int(client_port)))
        print(f'Created client udp server - {server_address} ...')

        server_host, server_port = server_address
        client_tcp_connection.sendall(self.tcp_packet(f'{server_host}:{server_port}'))
        print('Sent udp server info to client ...')
        print('Started sending message to client udp server')

        while True:
            try:
                time.sleep(3)
                self.send_udp_message_attached(
                    sending_socket=udp_client_server,
                    message=f'Time is - {time.time()}'
                )
            except KeyboardInterrupt:
                self.tcp_connection.close()
                udp_client_server.close()
