import json
import threading
from typing import (
    Callable,
    Tuple, List
)
import socket

import pygame

from ..events import (
    CUSTOM_EVENTS,
    create_create_event,
    create_remove_event
)
from ..game import GameContext
from ..objects.tank import (
    Tank,
    SpawnTankAnimation
)
from ..commands import DumpEvents
from ..enum import (
    TankControl,
    TankCodes,
    Identifier,
    AnimationCodes
)
from .session import (
    ServerNetworkManager,
    ServerSession
)
from .acceptor import ConnectionAcceptor
from ..map_manager import ServerTestMap


class ServerGame(GameContext):

    def __init__(
            self,
            host: str,
            tcp_port: int,
            manager_receive_function: Callable[[socket.socket], bytes] = None,
            frames_per_second: int = 60,
            password: str = None,
            **kwargs
    ):
        self.host = host
        self.tcp_port = tcp_port
        self.manager_receive_function = manager_receive_function or ServerNetworkManager.receive_tcp_message_full
        self.frames_per_second = frames_per_second
        self.password = password
        super(ServerGame, self).__init__(**kwargs)

    def init_map(self) -> None:
        self.map = ServerTestMap(game_state=self)
        self.map.init_map()
        manager = ServerNetworkManager(
            host=self.host,
            tcp_port=self.tcp_port,
            receive_function=self.manager_receive_function
        )
        self.session = ServerSession(manager=manager, server_password=self.password)
        self.acceptor = ConnectionAcceptor(session=self.session)
        self.tanks = dict()
        self.queue_event = threading.Event()

    @staticmethod
    def dump_events(events: List[pygame.event.Event]) -> str:
        command = DumpEvents(event_list=events)
        command.run()
        return command.dumped

    def broadcast_events_tcp(self, events: List[pygame.event.Event]) -> None:
        dumped_events = self.dump_events(events=events)
        self.session.broadcast_tcp(
            packet=self.session.manager.tcp_packet(dumped_events)
        )

    def handle_connection(self, connection: socket.socket, address: Tuple[str, int]) -> None:
        try:
            end_connection, connection_established = self.session.start_accept_thread(
                connection=connection,
                address=address,
                map_data=self.map.dump_map(),
                frame_rate=self.frames_per_second,
            )

            connection_established.wait()

            if not self.session.authenticated(connection):
                return

            current_tank = Tank.create_from_code(
                code=TankCodes.SIMPLE_GOLD_TANK,
                start_x_cord=144,
                start_y_cord=144,
                game_state=self,
            )
            current_tank.can_control = TankControl.CANT
            animation = SpawnTankAnimation.create_from_code(
                code=AnimationCodes.SPAWN,
                start_x_cord=current_tank.rect.x,
                start_y_cord=current_tank.rect.y,
                game_state=self,
                tank=current_tank,
                next_frame_delay=500
            )
            self.map.objects.add(animation)

            # send changes to the rest of servers
            # via udp server

            spawn_event = create_create_event(
                identifier=Identifier.ANIMATION,
                object_identifier=AnimationCodes.SPAWN,
                position_y=animation.rect.y,
                position_x=animation.rect.x,
                attributes={
                    'times': animation.times,
                    'next_frame_delay': 500
                }
            )

            self.broadcast_events_tcp(events=[spawn_event])

            animation.spawned.wait()

            self.broadcast_events_tcp(events=animation.events)

            self.queue_event.wait()

            client_uuid = self.session.auth_journal[address]
            self.tanks[client_uuid] = current_tank

            end_connection.wait()

            print(f'Player {client_uuid} | {address} is disconnected')

            self.queue_event.wait()
            self.tanks.pop(client_uuid)
            self.map.objects.remove(current_tank)

            remove_event = create_remove_event(
                object_x=current_tank.rect.x,
                object_y=current_tank.rect.y
            )

            self.broadcast_events_tcp(events=[remove_event])

        except Exception as e:
            print('Error occurred')
            raise e

    def handle_client_events(self, client_uuid: str) -> None:
        try:
            current_tank = self.tanks[client_uuid]
        except KeyError:
            return

        pressed = self.session.receive_client_keypress_not_blocking(
            client_uuid=client_uuid
        )

        if not pressed or (pressed and isinstance(json.loads(pressed), int)):
            return

        pressed = {int(key): value for key, value in json.loads(pressed).items()}
        key = current_tank.get_one_pressed(pressed=pressed)

        if key:
            direction = key[0][0]
            distance = Tank.get_speed_values(speed=5)[direction]
            current_tank.move(direction=direction, distance=distance)

        if pressed[pygame.K_SPACE]:
            current_tank.spawn_bullet()

        current_tank.decrease_cooldown()

    def run(self) -> None:
        pygame.init()

        self.running = True
        acceptor = threading.Thread(
            target=self.acceptor.start_acceptor,
            args=(self.handle_connection, )
        )
        acceptor.start()

        while self.running:

            try:
                self.queue_event.clear()

                for client_uuid in self.session.udp_connections.keys():
                    self.handle_client_events(client_uuid=client_uuid)

                event_list = pygame.event.get(eventtype=CUSTOM_EVENTS)
                dumped_events = self.dump_events(events=event_list)

                if event_list:
                    self.session.broadcast_udp(
                        message=dumped_events,
                        function=self.session.manager.send_udp_message_attached
                    )

                self.queue_event.set()
                self.render(screen=self.screen)
                self.map.objects.update()
                self.clock.tick(self.frames_per_second)

            except KeyboardInterrupt:
                self.running = False
                self.acceptor.close()

        acceptor.join()
