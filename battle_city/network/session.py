from __future__ import annotations
import errno
import json
import socket
import threading
import uuid
from typing import (
    Dict,
    Callable,
    TypeVar,
    Union, Any, List, Tuple
)
from typing_extensions import ParamSpec

from .network_manager import (
    ClientNetworkManager,
    ServerNetworkManager
)
from ..enum import Actions


P = ParamSpec('P')
V = TypeVar('V')


def supress_blocking(function: Callable[P, V]) -> Callable[P, V]:
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> V:
        try:
            return function(*args, **kwargs)
        except IOError as error:
            if error.errno != errno.EAGAIN and error.errno != errno.EWOULDBLOCK:
                raise error
    return wrapper


class ClientSession:

    def __init__(
            self,
            manager: ClientNetworkManager,
            server_password: str = None,
    ):
        self.manager = manager
        self.server_password = server_password
        self.udp_connection: socket.socket = None
        self.ping_event = threading.Event()
        self.ping_event.set()

    def send_command(
            self,
            action: int,
            message: str,
            retries: int = 5,
            last_response: Any = None
    ) -> Union[str, None]:

        if retries <= 0:
            raise ConnectionError(f'Error on sending action {action} - {last_response[1:]}')

        self.manager.send_tcp_message(f'{action}{message}')
        try:
            received = self.manager.receive_tcp_message(
                receiving_socket=self.manager.tcp_connection
            ).decode(self.manager.encoding)
        except AttributeError:
            return self.send_command(
                action=action,
                message=message,
                retries=retries-1,
                last_response='Message not received'
            )
        received_action = int(received[0])

        if received_action == Actions.ERROR:
            return self.send_command(
                action=action,
                message=message,
                retries=retries-1,
                last_response=received
            )

        elif received_action == Actions.ACCEPTED:
            return received[1:]

    def ask_uuid(self) -> None:
        self.uuid = self.send_command(
            action=Actions.GET_UUID,
            message=self.server_password if self.server_password else '',
            retries=1
        )

    def ask_udp_server(self) -> None:
        udp_server, udp_server_address = self.manager.create_udp_server(
            binding_host=self.manager.host
        )
        client_host, client_port = udp_server_address
        main_udp_address = self.send_command(
            action=Actions.SET_UDP,
            message=f'{client_host}:{client_port}',
            retries=3
        )
        main_host, main_port = main_udp_address.split(':')
        udp_server.connect((main_host, int(main_port)))
        self.udp_connection = udp_server

    def ask_map_data(self) -> None:
        self.map_data = self.send_command(
            action=Actions.GET_MAP,
            message='',
            retries=3
        )

    def ask_frame_rate(self) -> None:
        self.frame_rate = self.send_command(
            action=Actions.GET_FRAME_RATE,
            message='',
            retries=3
        )

    def ping(self) -> None:
        self.ping_event.clear()
        self.send_command(
            action=Actions.PING,
            message='Ping',
            retries=1
        )
        self.ping_event.set()

    def start_events(self) -> None:
        self.manager.send_tcp_message(f'{Actions.GET_EVENTS}')

    def send_pressed(self, pressed: Dict[int, int]) -> None:
        self.manager.send_udp_message_attached(
            sending_socket=self.udp_connection,
            message=json.dumps(pressed),
        )

    @supress_blocking
    def receive_events_udp_not_blocking(self) -> Union[str, None]:
        if self.udp_connection.getblocking():
            self.udp_connection.setblocking(False)

        events = self.manager.receive_udp_message_full_attached(
            receiving_socket=self.udp_connection
        )
        if events:
            return events.decode(self.manager.encoding)

    @supress_blocking
    def receive_events_tcp_not_blocking(self) -> Union[str, None]:
        if self.manager.tcp_connection.getblocking():
            self.manager.tcp_connection.setblocking(False)

        self.ping_event.wait()
        tank_event = self.manager.receive_tcp_message(
            receiving_socket=self.manager.tcp_connection
        )
        self.manager.tcp_connection.setblocking(True)
        if tank_event:
            return tank_event.decode(self.manager.encoding)


SESSION_ACTION_HANDLERS = []


def action_handler(action: int, authenticated: bool = True) -> Callable[
    [Callable[P, bool]], Callable[P, bool]
]:
    def function_wrapper(function: Callable[P, bool]) -> Callable[P, bool]:
        def args_wrapper(
                self: ServerSession,
                connection: socket.socket,
                message: str,
                **kwargs: P.kwargs
        ) -> bool:
            is_authenticated = self.authenticated(client_connection=connection)

            if authenticated and not is_authenticated:
                self.response(
                    connection=connection,
                    message=f'{Actions.ERROR}Not authorized'
                )
                return True

            if not message:
                return True

            if int(message[0]) == action:
                return function(self, connection, message[1:], **kwargs)

        SESSION_ACTION_HANDLERS.append(args_wrapper)

        return args_wrapper
    return function_wrapper


class ServerSession:

    def __init__(
            self,
            manager: ServerNetworkManager,
            server_password: str = None,
    ):
        self.manager = manager
        self.server_password = server_password or ''
        self.tcp_connections = dict()
        self.udp_connections = dict()
        self.auth_journal = dict()
        self.action_handlers = []
        self.addresses = dict()
        self.accepting_threads: List[threading.Thread] = []

    def close(self) -> None:
        for thread in self.accepting_threads:
            thread.join()

    def start_accept_thread(
            self,
            connection: socket.socket,
            address: Tuple[str, int],
            **kwargs
    ) -> Tuple[threading.Event, threading.Event]:
        end_connection_event = threading.Event()
        connection_established = threading.Event()
        thread = threading.Thread(
            target=self.acceptor,
            args=(
                connection,
                address,
                end_connection_event,
                connection_established
            ),
            kwargs=kwargs
        )
        self.accepting_threads.append(thread)
        thread.start()
        return end_connection_event, connection_established

    def unregister(self, client_uuid: str) -> None:
        client_tcp = self.tcp_connections.pop(client_uuid)
        address = self.addresses.pop(client_tcp)
        client_udp = self.udp_connections.pop(client_uuid)
        self.auth_journal.pop(address)

        client_udp.close()
        client_tcp.close()

    def authenticated(self, client_connection: socket.socket) -> bool:
        address = self.addresses.get(client_connection)
        return address in self.auth_journal

    def response(self, connection: socket.socket, message: str) -> None:
        connection.sendall(self.manager.tcp_packet(message))

    @action_handler(action=Actions.GET_UUID, authenticated=False)
    def authenticate(
            self,
            connection: socket.socket,
            message: str,
            **kwargs
    ) -> bool:

        if self.authenticated(client_connection=connection):
            to_send = f'{Actions.ERROR}Already authorized'
            terminate_loop = True

        elif message == self.server_password:
            client_uuid = str(uuid.uuid4())
            self.tcp_connections[client_uuid] = connection
            self.auth_journal[kwargs['address']] = client_uuid
            self.addresses[connection] = kwargs['address']
            to_send = f'{Actions.ACCEPTED}{client_uuid}'
            terminate_loop = False

        else:
            to_send = f'{Actions.ERROR}Invalid password'
            terminate_loop = True

        self.response(connection=connection, message=to_send)

        return terminate_loop

    @action_handler(action=Actions.SET_UDP)
    def establish_udp(
            self,
            connection: socket.socket,
            message: str,
            **kwargs
    ) -> bool:
        client_uuid = self.auth_journal[kwargs['address']]
        client_host, client_port = message.split(':')
        client_udp_server, address = self.manager.create_udp_server(
            binding_host=self.manager.host
        )
        self.udp_connections[client_uuid] = client_udp_server
        self.tcp_connections[client_uuid] = connection
        main_host, main_port = address

        client_udp_server.connect((client_host, int(client_port)))
        self.response(
            connection=connection,
            message=f'{Actions.ACCEPTED}{main_host}:{main_port}'
        )
        return False

    @action_handler(action=Actions.GET_MAP)
    def send_map(
            self,
            connection: socket.socket,
            message: str,
            **kwargs
    ) -> bool:
        map_data = kwargs.get('map_data', '')
        self.response(
            connection=connection,
            message=f'{Actions.ACCEPTED}{map_data}'
        )
        return False

    @action_handler(action=Actions.GET_FRAME_RATE)
    def send_frame_rate(
            self,
            connection: socket.socket,
            message: str,
            **kwargs
    ) -> bool:
        frame_rate = str(kwargs.get('frame_rate', 30))
        self.response(
            connection=connection,
            message=f'{Actions.ACCEPTED}{frame_rate}'
        )
        kwargs.get('connection_established').set()
        return False

    @action_handler(action=Actions.PING)
    def ping(self, connection: socket.socket, message: str, **_) -> bool:
        self.response(
            connection=connection,
            message=f'{Actions.ACCEPTED}Pong'
        )
        return False

    def acceptor(
            self,
            client_connection: socket.socket,
            address: Tuple[str, int],
            end_connection: threading.Event,
            connection_established: threading.Event,
            **kwargs
    ) -> None:
        terminate_loop = False
        kwargs.update({
            'connection_established': connection_established,
            'address': address
        })

        print(f'New client accepted - {address}')

        while not terminate_loop:

            message = self.manager.receive_tcp_message(
                receiving_socket=client_connection
            )

            if message is None:
                break

            for handler in SESSION_ACTION_HANDLERS:
                terminate_loop = handler(
                    self,
                    client_connection,
                    message.decode(self.manager.encoding),
                    **kwargs
                )

        client_uuid = self.auth_journal.get(address)

        if client_uuid:
            self.unregister(client_uuid=client_uuid)

        end_connection.set()

    def broadcast_tcp(self, packet: bytes) -> None:
        for connection in self.tcp_connections.values():
            connection.sendall(packet)

    def broadcast_udp(
            self,
            message: str,
            function: Callable[[socket.socket, str], None]
    ) -> None:
        for udp in self.udp_connections.values():
            function(udp, message)

    @supress_blocking
    def receive_client_keypress_not_blocking(self, client_uuid: str) -> Union[str, None]:
        udp_connection = self.udp_connections[client_uuid]

        if udp_connection.getblocking():
            udp_connection.setblocking(False)

        keypress = self.manager.receive_udp_message_full_attached(
            receiving_socket=udp_connection
        )

        if keypress:
            return keypress.decode(self.manager.encoding)