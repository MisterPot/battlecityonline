import socket
import threading
from typing import (
    Callable,
    Tuple,
    List
)

from .session import ServerSession


class ConnectionAcceptor:

    def __init__(self, session: ServerSession):
        self.session = session
        self.accepting_threads: List[threading.Thread] = []

    def start_acceptor(
            self, connection_handler: Callable[[socket.socket, Tuple[str, int]], None],
    ) -> None:
        print('Start accepting')
        while True:
            try:
                connection, address = self.session.manager.tcp_connection.accept()
            except OSError:
                break
            thread = threading.Thread(target=connection_handler, args=(connection, address))

            self.accepting_threads.append(thread)
            thread.start()
        print('Accepting stopped')

    def close(self) -> None:
        print('Closing connection')
        self.session.manager.tcp_connection.close()
        for thread in self.accepting_threads:
            thread.join()
